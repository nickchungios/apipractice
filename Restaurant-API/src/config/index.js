export default {
  "port": 3005,
  "mongourl": "mongodb://localhost:27017/restaurant-api",
  "bodyLimit": "100kb"
}
