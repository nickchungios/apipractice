var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var VehcileSchema = new Schema({
  make: String,
  model: String,
  color: String
});

module.exports = mongoose.model('Vehicle', VehcileSchema);
