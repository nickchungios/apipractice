//
//  ReviewCell.swift
//  api-client
//
//  Created by cjnora on 2018/2/12.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import UIKit

class ReviewCell: UITableViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var reviewTextLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    func configureCell(review: FoodTruckReview) {
        titleLabel.text = review.title
        reviewTextLabel.text = review.text
    }

}
