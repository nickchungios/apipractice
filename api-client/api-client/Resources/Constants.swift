//
//  Constants.swift
//  api-client
//
//  Created by cjnora on 2018/2/7.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import Foundation

//Callbacks
//Typealias for callbacks used in Data Service
typealias callback = (_ success: Bool) -> ()

//Base URL
let BASE_API_URL = "http://localhost:3005/api/v1"

//GET all food trucks
let GET_ALL_FT_URL = "\(BASE_API_URL)/foodtruck"

//GET all reviews for a specfic food truck
let GET_ALL_FT_Reviews = "\(BASE_API_URL)/foodtruck/reviews"

//POST add a new Food Truck
let POST_ADD_NEW_TRUCK = "\(BASE_API_URL)/foodtruck/add"

//POST add a review for a specific food truck
let POST_ADD_NEW_REVIEW = "\(BASE_API_URL)/foodtruck/reviews/add"

//PUT update a food truck
let PUT_UPDATE_FOOD_TRUCK = "\(BASE_API_URL)/foodtruck"

//DELETE delete a food truck
let DELETE_A_FOOD_TRUCK = "\(BASE_API_URL)/foodtruck"

//Boolean auth UserDefaults keys
let DEFAULTS_REGISTERED = "isRegistered"
let DEFAULT_AUTHENTICATED = "isAuthenticate"

//Auth Email
let DEFAULTS_EMAIL = "email"

//Auth Token
let DEFAULTS_TOKEN = "authToken"

//REGISTER url
let POST_REGISTER_ACCT = "\(BASE_API_URL)/account/register"

let POST_LOGIN_ACCT = "\(BASE_API_URL)/account/login"

//Notication
let NOTIF_UPDATE_FOOD_TRUCK = Notification.Name("updateFoodTruck")

//Selected Food Truck For Update View Controller
 var SELECTED_FOOD_TRUCK: FoodTruck?
