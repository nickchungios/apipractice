//
//  MainVC.swift
//  api-client
//
//  Created by cjnora on 2018/2/7.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import UIKit

class MainVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    var dataService = DataService.instance
    var authService = AuthService.instance
    
    var logInVC: LogInVC?
    
    //Swipe to delete
    var deleteFoodTruckIndexPath: NSIndexPath? = nil
    var foodTruckToDelete: FoodTruck?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataService.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        
        dataService.getAllFoodTrucks()
        
        //Auto regulate table view incorrect size in auto layout
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = UITableViewAutomaticDimension
    }
    
    func showLogInVC() {
        logInVC = LogInVC()
        logInVC?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        self.present(logInVC!, animated: true, completion: nil)
    }
    
    @IBAction func addButtonTapped(sender: UIButton) {
        if authService.isAuthenticated == true {
            performSegue(withIdentifier: "showAddTruckVC", sender: self)
        } else {
            showLogInVC()
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetailsVC" {
            if let indexPath = tableView.indexPathForSelectedRow {
                let destinationViewController = segue.destination as! DetailsVC
                destinationViewController.selectedFoodTruck = DataService.instance.foodTrucks[indexPath.row]
            }
        }
    }
    
    func showAlert(with title:String?, message:String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
    
    //Swipe to delete
    func confirmDelete(foodTruck: FoodTruck) {
        let alert = UIAlertController(title: "Delete Food Truck", message: "Are you sure you want to permanently delete \(foodTruck.name)?", preferredStyle: .actionSheet)
        
        let DeleteAction = UIAlertAction(title: "Delete", style: .destructive, handler: handleDeleteFoodTruck)
        let CancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: cancelDeleteFoodTruck)
        
        alert.addAction(DeleteAction)
        foodTruckToDelete = foodTruck
        alert.addAction(CancelAction)
        
        // Support display in iPad
        alert.popoverPresentationController?.sourceView = self.view
        alert.popoverPresentationController?.sourceRect = CGRect(x: self.view.bounds.size.width / 2.0, y: self.view.bounds.size.height / 2.0, width: 1.0, height: 1.0)
        
        self.present(alert, animated: true, completion: nil)
    }
    func handleDeleteFoodTruck(alertAction: UIAlertAction!) -> Void {
        dataService.deleteAFoodTruck(for: foodTruckToDelete!, completion: { (Success) in
            if Success {
                if let indexPath = self.deleteFoodTruckIndexPath {
                    OperationQueue.main.addOperation{
                    self.tableView.beginUpdates()
                    
                    self.dataService.foodTrucks.remove(at: indexPath.row)
                    
                    // Note that indexPath is wrapped in an array:  [indexPath]
                    self.tableView.deleteRows(at: [indexPath as IndexPath], with: .automatic)
                    
                    self.deleteFoodTruckIndexPath = nil
                    
                    self.tableView.endUpdates()
                    }
                }
            } else {
                print("Deletion Unsuccessful.")
                self.showAlert(with: "Error", message: "Something wrong for Deletion.")
            }
        })
    }
    
    func cancelDeleteFoodTruck(alertAction: UIAlertAction!) {
        deleteFoodTruckIndexPath = nil
    }
}

extension MainVC: DataServiceDelegate {
    func truckLoaded() {
        OperationQueue.main.addOperation {
            self.tableView.reloadData()
        }
    }
    
    func reviewsLoaded() {
        // Do nothing
    }
}

extension MainVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }//<----Auto regulate table view incorrect size in auto layout
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataService.foodTrucks.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "FoodTruckCell", for: indexPath) as? FoodTruckCell {
            cell.configureCell(truck: dataService.foodTrucks[indexPath.row])
            return cell
        } else {
            return UITableViewCell()
        }
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            deleteFoodTruckIndexPath = indexPath as NSIndexPath
            let foodTruckToDelete = dataService.foodTrucks[indexPath.row]
            confirmDelete(foodTruck: foodTruckToDelete)
        }
    }
}
