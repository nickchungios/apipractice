//
//  SetFoodTruckLocationVC.swift
//  api-client
//
//  Created by cjnora on 2018/2/15.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import UIKit
import MapKit

class SetFoodTruckLocationVC: UIViewController {
    
    //Outlets
    @IBOutlet weak var nameLbl: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var mapView: MKMapView!
    
    //Variables
    var selectedFoodTruck: FoodTruck?
    var searchController: UISearchController!
    var annotation:MKAnnotation!
    var localSearchRequest:MKLocalSearchRequest!
    var localSearch:MKLocalSearch!
    var localSearchResponse:MKLocalSearchResponse!
    var error:NSError!
    var pointAnnotation:MKPointAnnotation!
    var pinAnnotationView:MKPinAnnotationView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        selectedFoodTruck = SELECTED_FOOD_TRUCK
        setupView()
        setupSearchBar()
    }
    
    //Set up
    func setupView() {
        nameLbl.text = selectedFoodTruck?.name
        mapView.addAnnotation(selectedFoodTruck!)
        centerMapOnLocation(CLLocation(latitude: selectedFoodTruck!.lat, longitude: selectedFoodTruck!.long))
    }
    
    func setupSearchBar() {
        searchController = UISearchController(searchResultsController: nil)
        self.searchController.searchBar.delegate = self
    }
    
    //action
    @IBAction func confirmBtnPressed(sender: UIButton) {
        
    }
    
    @IBAction func backBtnPressed(sender: UIButton) {
        dismissViewController()
    }
    
    //function
    func dismissViewController() {
        self.dismiss(animated: true, completion: nil)
    }
    
    func centerMapOnLocation(_ location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(selectedFoodTruck!.coordinate, 1000, 1000)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    func showAlert(with title:String?, message:String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        self.present(alertController, animated: true, completion: nil)
    }
    
}

extension SetFoodTruckLocationVC: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        localSearchRequest = MKLocalSearchRequest()
        localSearchRequest.naturalLanguageQuery = searchBar.text
        localSearch = MKLocalSearch(request: localSearchRequest)
        
        localSearch.start { (localSearchResponse, error) in
            if error != nil {
                print("Error occured in search: \(error!.localizedDescription)")
            } else if localSearchResponse == nil {
                self.showAlert(with: "Error", message: "Place not found")
                return
            } else {
                self.pointAnnotation = MKPointAnnotation()
                self.pointAnnotation.title = searchBar.text
                self.pointAnnotation.coordinate = CLLocationCoordinate2D(latitude:localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude)
                
                
                self.pinAnnotationView = MKPinAnnotationView(annotation: self.pointAnnotation, reuseIdentifier: nil)
                self.mapView.centerCoordinate = self.pointAnnotation.coordinate
                self.mapView.addAnnotation(self.pinAnnotationView.annotation!)
                
                self.centerMapOnLocation(CLLocation(latitude:localSearchResponse!.boundingRegion.center.latitude, longitude: localSearchResponse!.boundingRegion.center.longitude))
                print("Find location Successfully.")
            }
        }
    }
}
