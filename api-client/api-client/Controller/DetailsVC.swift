//
//  DetailsVC.swift
//  api-client
//
//  Created by cjnora on 2018/2/11.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import UIKit
import MapKit

class DetailsVC: UIViewController {
    
    var selectedFoodTruck: FoodTruck?
    var logInVC: LogInVC?
    var updateView: updateVC?
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var foodTypeLabel: UILabel!
    @IBOutlet weak var avgCostLabel: UILabel!
    @IBOutlet weak var mapView: MKMapView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        NotificationCenter.default.addObserver(self, selector: #selector(DetailsVC.foodTruckUpdated(_:)), name: NOTIF_UPDATE_FOOD_TRUCK, object: nil)

    }
    
    func setupView() {
        nameLabel.text = selectedFoodTruck!.name
        foodTypeLabel.text = selectedFoodTruck!.foodType
        avgCostLabel.text = "\(selectedFoodTruck!.avgCost)"
        
        mapView.addAnnotation(selectedFoodTruck!)
        centerMapOnLocation(CLLocation(latitude: selectedFoodTruck!.lat, longitude: selectedFoodTruck!.long))

    }
    
    @objc func foodTruckUpdated(_ notif: Notification) {
        mapView.removeAnnotation(selectedFoodTruck!)
        selectedFoodTruck = SELECTED_FOOD_TRUCK
        
        OperationQueue.main.addOperation {
            self.setupView()
        }
    }

    func centerMapOnLocation(_ location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(selectedFoodTruck!.coordinate, 1000, 1000)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    @IBAction func reviewsButtonTapped(sender: UIButton) {
        performSegue(withIdentifier: "showReviewVC", sender: self)
    }
    
    @IBAction func addReviewButtonTapped(sender: UIButton) {
        if AuthService.instance.isAuthenticated == true {
            performSegue(withIdentifier: "showAddReviewVC", sender: self)
        } else {
            showLogInVC()
        }
    }
    
    @IBAction func backButtonTapped(sender: UIButton) {
        _ = navigationController?.popViewController(animated: true)
    }
    
    @IBAction func updateButtonTapped(sender: UIButton) {
        if AuthService.instance.isAuthenticated == true {
            SELECTED_FOOD_TRUCK = selectedFoodTruck
            showUpdateVC()
        } else {
            showLogInVC()
        }
    }
    
    func showLogInVC() {
        logInVC = LogInVC()
        logInVC?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        self.present(logInVC!, animated: true, completion: nil)
    }
    
    func showUpdateVC() {
        updateView = updateVC()
        updateView?.modalPresentationStyle = UIModalPresentationStyle.formSheet
        self.present(updateView!, animated: true, completion: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showReviewVC" {
            let destinationViewController = segue.destination as! ReviewsVC
            destinationViewController.selectedFoodTruck = selectedFoodTruck
        } else if segue.identifier == "showAddReviewVC" {
            let destinationViewController = segue.destination as! AddReviewVC
            destinationViewController.selectedFoodTruck = selectedFoodTruck
        }
    }

}
