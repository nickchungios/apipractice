//
//  updateVC.swift
//  api-client
//
//  Created by cjnora on 2018/2/13.
//  Copyright © 2018年 nickchunglolz. All rights reserved.
//

import UIKit

class updateVC: UIViewController {
    
    static let instance = updateVC()
    
    var selectedFoodTruck: FoodTruck?
    
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var foodTypeField: UITextField!
    @IBOutlet weak var avgCostField: UITextField!
    @IBOutlet weak var latitudeField: UITextField!
    @IBOutlet weak var longitudeField: UITextField!
    @IBOutlet weak var spinner: UIActivityIndicatorView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUpView()
    }
    
    func setUpView() {
        spinner.isHidden = true
        spinner.stopAnimating()
        
        selectedFoodTruck = SELECTED_FOOD_TRUCK
        nameField.text = selectedFoodTruck?.name
        foodTypeField.text = selectedFoodTruck?.foodType
        avgCostField.text = "\(selectedFoodTruck!.avgCost)"
        latitudeField.text = "\(selectedFoodTruck!.lat)"
        longitudeField.text = "\(selectedFoodTruck!.long)"
    }
    
    @IBAction func locationBtnPressed(sender: UIButton) {
        let locationVC = SetFoodTruckLocationVC()
        locationVC.modalPresentationStyle = UIModalPresentationStyle.formSheet
        self.present(locationVC, animated: true, completion: nil)
    }
    
    @IBAction func updateButtonTapped(sender: UIButton) {
        spinner.isHidden = false
        spinner.startAnimating()
        guard let name = nameField.text, nameField.text != "" else {
            showAlert(with: "Error",message: "Please enter a food truck name")
            return }
        guard let foodtype = foodTypeField.text, foodTypeField.text != "" else {
            showAlert(with: "Error",message: "Please enter food type")
            return }
        guard let avgcost = Double(avgCostField.text!), avgCostField.text != "" else {
            showAlert(with: "Error",message: "Please enter average cost")
            return }
        guard let latitude = Double(latitudeField.text!), latitudeField.text != "" else {
            showAlert(with: "Error",message: "Please enter latitude")
            return }
        guard let longitude = Double(longitudeField.text!), longitudeField.text != "" else {
            showAlert(with: "Error",message: "Please enter longitude")
            return }
        DataService.instance.updateFoodTruck(for: selectedFoodTruck!, name: name, foodtype: foodtype, avgcost: avgcost, latitude: latitude, longitude: longitude) { (Success) in
            if Success {
                print("updating Successfully.")
                SELECTED_FOOD_TRUCK?.name = name
                SELECTED_FOOD_TRUCK?.foodType = foodtype
                SELECTED_FOOD_TRUCK?.avgCost = avgcost
                SELECTED_FOOD_TRUCK?.lat = latitude
                SELECTED_FOOD_TRUCK?.long = longitude
                NotificationCenter.default.post(name: NOTIF_UPDATE_FOOD_TRUCK, object: nil)
                self.spinner.stopAnimating()
                self.spinner.isHidden = true
                self.dismiss(animated: true, completion: nil)
            } else {
                print("updating Unsuccessful.")
                self.showAlert(with: "Error", message: "Something wrong for Updating.")
            }
        }
    }
    
    @IBAction func cancelButtonTapped(sender: UIButton) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func showAlert(with title:String?, message:String?) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alertController.addAction(okAction)
        present(alertController, animated: true, completion: nil)
    }
}
